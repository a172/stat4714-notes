# Introduction

This is notes for STAT 4714 with George Tarrel.
I'm in the Tuesday/Thursday section, so that is what the notes will mostly
follow.

# Contributing

The more contributions we have, the better the notes will be.
All contributions, large and small, are appreciated.

The document text and structure is written in Markdown.
The math is written in $$\KaTeX$$.
Graphs are generated with function-plot.

Please open pull request, or open issues on GitLab.
If you have never heard of git, then feel free to message me (@a172) on Discord
with things that need fixed or suggestions.

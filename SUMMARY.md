# Table of contents

## Lectures
* [Through 2020-09-09&#x2197;](https://docs.google.com/document/d/1RabVNTRmXeO6cc3TlmPTqxFhzIyRO5-MjWZctjetke0)
* [2020-09-18](lectures/2020-09-18.md)
    * [Expectation (Discrete)](lectures/2020-09-18.md#expectation-discrete)
    * [Variance](lectures/2020-09-18.md#variance)
* [2020-09-22](lectures/2020-09-22.md)
    * [Variance (Cont)](lectures/2020-09-22.md#variance-cont)
    * [Standard Deviation](lectures/2020-09-22.md#standard-deviation)
    * [Probability Generating Function](lectures/2020-09-22.md#probability-generating-function)
* [2020-09-29](lectures/2020-09-29.md)
    * [Probability Generating Function](lectures/2020-09-29.md#probability-generating-function)
    * [Binomial Random Variables](lectures/2020-09-29.md#binomial-random-variables)
* [2020-10-01](lectures/2020-10-01.md)
    * [Binomial Random Variables](lectures/2020-10-01.md#binomial-random-variables)
* [2020-10-06](lectures/2020-10-06.md)
    * [Approximating Binomials](lectures/2020-10-06.md#approximating-binomials)
    * [Poisson Random Variables](lectures/2020-10-06.md#poisson-random-variables)
    * [Continuous Random Variables](lectures/2020-10-06.md#continuous-random-variables)
    * [Exponential Family (reliability)](lectures/2020-10-06.md#exponential-family)
* [2020-10-08](lectures/2020-10-08.md)
    * [Constant Hazard Systems](lectures/2020-10-08.md#constant-hazard-systems)
    * [Exponential Family (continued)](lectures/2020-10-08.md#exponential-family-continued)
* [2020-10-13](lectures/2020-10-13.md)
    * [Density](lectures/2020-10-13.md#density)
    * [Variance](lectures/2020-10-13.md#variance)
    * [What does density really mean?](lectures/2020-10-13.md#what-does-density-really-mean)
    * [Reliability of systems](lectures/2020-10-13.md#reliability-of-systems)
* [2020-10-15](lectures/2020-10-15.md)
    * [Hazard](lectures/2020-10-15.md#hazard)
    * [Weibull Random Variable](lectures/2020-10-15.md#weibull-family)
    * [Reliability](lectures/2020-10-15.md#reliability)
* [2020-10-20](lectures/2020-10-20.md)
    * [The Poisson(λ) process](lectures/2020-10-20.md#the-poissonλ-process)
    * [Moment Generating Function](lectures/2020-10-20.md#moment-generating-function)
* [2020-10-22](lectures/2020-10-22.md)
    * [Moment Generating Function](lectures/2020-10-22.md#moment-generating-function)
* [2020-10-27](lectures/2020-10-27.md)
    * [Series System](lectures/2020-10-27.md#series-system)
    * [Parallel System](lectures/2020-10-27.md#parallel-system)
    * [Summary of compound systems](lectures/2020-10-27.md#summary-of-compound-systems)
* [2020-11-03](lectures/2020-11-03.md)
    * [PGF vs MGF](lectures/2020-11-03.md#pgf-vs-mgf)
    * [Last comments on Reliability](lectures/2020-11-03.md#last-comments-on-reliability)
    * [Normal Probability Laws](lectures/2020-11-03.md#normal-probability-laws)
* [2020-11-05](lectures/2020-11-05.md)
    * [Standardization (Gamma)](lectures/2020-11-05.md#standardization)
* [2020-11-10](lectures/2020-11-10.md)
    * [Normal Approximation](lectures/2020-11-10.md#normal-approximation)
    * [Central Limit Theorem](lectures/2020-11-10.md#central-limit-theorem)
* [2020-11-12](lectures/2020-11-12.md)
* [2020-11-17](lectures/2020-11-17.md)
* [2020-11-19](lectures/2020-11-19.md)
* [2020-12-01](lectures/2020-12-01.md)
* [2020-12-03](lectures/2020-12-03.md)
* [2020-12-08](lectures/2020-12-08.md)

## Meta
* [Introduction](README.md)
* [Homework templates](hw.md)
* [Source&#x2197;](https://gitlab.com/a172/stat4714-notes)
* [Markdown syntax&#x2197;](https://www.markdownguide.org/basic-syntax/)
* [KaTeX supported functions&#x2197;](https://katex.org/docs/supported.html)
* [Function plot examples&#x2197;](https://mauriciopoppe.github.io/function-plot)
* [Function plot options&#x2197;](https://mauriciopoppe.github.io/function-plot/docs/interfaces/_src_types_.functionplotoptions.html)
* [Discord server&#x2197;](https://discord.gg/KBKV6Gs)

# Homework templates

Provided by Wizard of Paws on Discord.

1. [LaTeX][hw1.tex] / [PDF][hw1.pdf]
1. [LaTeX][hw2.tex] / [PDF][hw2.pdf]
1. [LaTeX][hw3.tex] / [PDF][hw3.pdf]
1. [LaTeX][hw4.tex] / [PDF][hw4.pdf]
1. [LaTeX][hw5.tex] / [PDF][hw5.pdf]
1. [LaTeX][hw6.tex] / [PDF][hw6.pdf]
1. [LaTeX][hw7.tex] / [PDF][hw7.pdf]
1. [LaTeX][hw8.tex] / [PDF][hw8.pdf]
1. [LaTeX][hw9.tex] / [PDF][hw9.pdf]
1. [LaTeX][hwa.tex] / [PDF][hwa.pdf]
1. [LaTeX][hwb.tex] / [PDF][hwb.pdf]
1. [LaTeX][hwc.tex] / [PDF][hwc.pdf]
1. [LaTeX][hwd.tex] / [PDF][hwd.pdf]

<!--
1. [LaTeX][hwd.tex] / [PDF][hwd.pdf]
-->

[hw1.tex]: hw/tex/STAT_4714_HW_1.tex
[hw2.tex]: hw/tex/STAT_4714_HW_2.tex
[hw3.tex]: hw/tex/STAT_4714_HW_3.tex
[hw4.tex]: hw/tex/STAT_4714_HW_4.tex
[hw5.tex]: hw/tex/STAT_4714_HW_5.tex
[hw6.tex]: hw/tex/STAT_4714_HW_6.tex
[hw7.tex]: hw/tex/STAT_4714_HW_7.tex
[hw8.tex]: hw/tex/STAT_4714_HW_8.tex
[hw9.tex]: hw/tex/STAT_4714_HW_9.tex
[hwa.tex]: hw/tex/STAT_4714_HW_10.tex
[hwb.tex]: hw/tex/STAT_4714_HW_11.tex
[hwc.tex]: hw/tex/STAT_4714_HW_12.tex
[hwd.tex]: hw/tex/STAT_4714_HW_13.tex
[hwe.tex]: hw/tex/STAT_4714_HW_14.tex

[hw1.pdf]: hw/pdf/STAT_4714_HW_1.pdf
[hw2.pdf]: hw/pdf/STAT_4714_HW_2.pdf
[hw3.pdf]: hw/pdf/STAT_4714_HW_3.pdf
[hw4.pdf]: hw/pdf/STAT_4714_HW_4.pdf
[hw5.pdf]: hw/pdf/STAT_4714_HW_5.pdf
[hw6.pdf]: hw/pdf/STAT_4714_HW_6.pdf
[hw7.pdf]: hw/pdf/STAT_4714_HW_7.pdf
[hw8.pdf]: hw/pdf/STAT_4714_HW_8.pdf
[hw9.pdf]: hw/pdf/STAT_4714_HW_9.pdf
[hwa.pdf]: hw/pdf/STAT_4714_HW_10.pdf
[hwb.pdf]: hw/pdf/STAT_4714_HW_11.pdf
[hwc.pdf]: hw/pdf/STAT_4714_HW_12.pdf
[hwd.pdf]: hw/pdf/STAT_4714_HW_13.pdf
[hwe.pdf]: hw/pdf/STAT_4714_HW_14.pdf
